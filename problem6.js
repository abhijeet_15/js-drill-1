function problem6(inventory) {

    let errorMessage;
    if(!Array.isArray(inventory)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(inventory.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    let BMWAndAudiCars = [];
    for(let index=0; index<inventory.length; index++) {
        if(inventory[index].car_make === "Audi" || inventory[index].car_make === "BMW") {
            BMWAndAudiCars.push(inventory[index]);
        }
    }

    return JSON.stringify(BMWAndAudiCars);
}

module.exports = problem6;