function problem5(carYears) {

    let errorMessage;
    if(!Array.isArray(carYears)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(carYears.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    let carYearsBelow2000 = [];
    for (let index=0; index<carYears.length; index++) {
        if (carYears[index] < 2000) {
            carYearsBelow2000.push(carYears[index]);
        }
    }
    let numberOfCarsBelow2000 = carYearsBelow2000.length;
    return {numberOfCars: numberOfCarsBelow2000, cars: carYearsBelow2000}
}

module.exports = problem5;