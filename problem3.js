function problem3(inventory) {

    let errorMessage;
    if(!Array.isArray(inventory)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(inventory.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    for(let index=0; index<inventory.length-1; index++) {
        let swapped = false;
        for(let jIndex=0; jIndex<inventory.length-index-1; jIndex++) {
            if(inventory[jIndex].car_model.toUpperCase() > inventory[jIndex+1].car_model.toUpperCase()) {
                let temp = inventory[jIndex]
                inventory[jIndex] = inventory[jIndex+1];
                inventory[jIndex+1] = temp;
                swapped = true;
            }
        }

        if(!swapped) {
            break;
        }
    }

    return inventory;
}

module.exports = problem3