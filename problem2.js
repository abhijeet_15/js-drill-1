function problem2(inventory) {

    let errorMessage;
    if(!Array.isArray(inventory)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(inventory.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    const lastIndex = inventory.length-1;
    const lastCar = inventory[lastIndex];
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`
}

module.exports = problem2