function problem1(inventory) {

    let errorMessage;
    if(!Array.isArray(inventory)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(inventory.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id == 33) {
            return `Car 33 is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`
        }
    }
}

module.exports = problem1