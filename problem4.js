function problem4(inventory) {

    let errorMessage;
    if(!Array.isArray(inventory)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(inventory.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    let carYears = [];
    for(let index=0; index<inventory.length; index++) {
        carYears.push(inventory[index].car_year);
    }
    return carYears;
}

module.exports = problem4